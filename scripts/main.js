function addZero(i) {
  return i > 9 ? i : `0${i}`;
}

function dayOfWeek(i) {
  switch (i) {
    case 0:
      return 'Sunday';
    case 1:
      return 'Monday';
    case 2:
      return 'Tuesday';
    case 3:
      return 'Wednesday';
    case 4:
      return 'Thursday';
    case 5:
      return 'Friday';
    case 6:
      return 'Saturday';
    default:
      break;
  }
  return new Error();
}

function monthOfYear(i) {
  switch (i) {
    case 0:
      return 'January';
    case 1:
      return 'February';
    case 2:
      return 'March';
    case 3:
      return 'April';
    case 4:
      return 'May';
    case 5:
      return 'June';
    case 6:
      return 'July';
    case 7:
      return 'August';
    case 8:
      return 'September';
    case 9:
      return 'October';
    case 10:
      return 'November';
    case 11:
      return 'December';
    default:
      break;
  }
  return new Error();
}

function setName() {
  const nameField = document.getElementById('name');
  localStorage.setItem('name', nameField.textContent);
}

function updateName() {
  const nameField = document.getElementById('name');
  if (localStorage.getItem('name')) nameField.textContent = localStorage.getItem('name');
  else nameField.textContent = '[Enter your name]';
}

function setFocus() {
  const focusField = document.getElementById('focus');
  localStorage.setItem('focus', focusField.textContent);
}

function updateFocus() {
  if (localStorage.getItem('focus')) document.getElementById('focus').textContent = localStorage.getItem('focus');
  else document.getElementById('focus').textContent = '[Enter your focus for today]';
}

function updateTime() {
  const dateTime = new Date();
  if (dateTime.getHours() < 12) {
    document.body.style.backgroundImage = "url('https://i.ibb.co/7vDLJFb/morning.jpg')";
    document.getElementById('greeting').textContent = 'Good morning, ';
  } else if (dateTime.getHours() < 18) {
    document.body.style.backgroundImage = "url('https://i.ibb.co/3mThcXc/afternoon.jpg')";
    document.getElementById('greeting').textContent = 'Good afternoon, ';
  } else {
    document.body.style.backgroundImage = "url('images/night.jpg')";
    document.getElementById('greeting').textContent = 'Good evening, ';
  }
  const hours = addZero(dateTime.getHours());
  const minutes = addZero(dateTime.getMinutes());
  const seconds = addZero(dateTime.getSeconds());
  document.getElementById('time').innerHTML = `${hours}:${minutes}:${seconds}`;
  document.getElementById('date').innerHTML = `${dayOfWeek(dateTime.getDay())}, ${dateTime.getDate()} ${monthOfYear(dateTime.getMonth())} ${dateTime.getFullYear()}`;
}

document.getElementById('name').addEventListener('blur', setName);
document.getElementById('focus').addEventListener('blur', setFocus);
updateName();
updateFocus();
setInterval(updateTime, 1000);
